package com.wade.webserver;

/**
 * Created by wade on 2016/9/23.
 */

public class Constant {
    public static final String SERVICE_NAME = "SimpleWebServer";
    /*
     * FLAG_TASK 指從 Activity 傳到 Service 的 TASK
     */
    public static final String FLAG_TASK      = "task";
    public static final int TASK_GET_ROOT_DIR = 0;
    public static final int TASK_SET_ROOT_DIR = 1; // 這個 task 需要額外的參數
    public static final int TASK_GET_PORT     = 2;
    public static final int TASK_SET_PORT     = 3;
    public static final int TASK_START_SERVER = 4;
    public static final int TASK_SERVER_STATE = 5;

    public static final String[] TASKSTR = {
            "TASK_GET_ROOT_DIR",
            "TASK_SET_ROOT_DIR",
            "TASK_GET_PORT",
            "TASK_SET_PORT",
            "TASK_START_SERVER",
            "TASK_STOP_SERVER",
            "TASK_SERVER_STATE",
    };

    /*
     * FLAG_RETURN 指從 Service 回傳到 Activity 的回傳值
     * RETURN 分三大類:
     *   LOG: 回傳與 Activity 互動的 log, 格式是 LOG:currentTimeMillis()-LOGMSG
     *   SERVER: 回傳 Server 的設定及狀態
     *   CLIENT: 回傳來自 Browser 的 request
     */
    public static final String FLAG_RETURN   = "LOG";
    public static final int RETURN_LOG       = 11;
    public static final int RETURN_SERVER    = 12;
    public static final int RETURN_CLIENT    = 13;

    /*
     * LOGTYPE_ 主要是區分與 Acitivity 之間 log type
     */
    public static final int LOGTYPE_ROOT_DIR = 21;
    public static final int LOGTYPE_STATUS   = 22;
    public static final int LOGTYPE_PORT     = 23;
}

