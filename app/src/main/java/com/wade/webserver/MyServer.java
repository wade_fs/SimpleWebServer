package com.wade.webserver;

import android.content.Intent;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.DatagramSocket;
import java.net.Socket;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

/**
 * Created by andrei on 7/30/15.
 */
public class MyServer extends NanoHTTPD {
    private final String TAG = "imWebService";

    private static int PORT = 8080;
    private static String rootDir = Environment.getExternalStorageDirectory().getPath();
    private static Boolean serverState = false;
    private static String uploadFileHTML =
            "<form method='post' enctype='multipart/form-data' action='/u'>"+
            "    Step 1. Choose a file: <input type='file' name='upload' /><br />"+
            "    Step 2. Click Send to upload file: <input type='submit' value='Send' /><br />"+
            "</form>";

    /*
     * 在此處理從 activity 透過 startService(intent) 中的 intent 傳遞過來的訊息
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        int taskFlag = intent.getIntExtra(Constant.FLAG_TASK, Constant.TASK_GET_ROOT_DIR);
        switch (taskFlag) {
            case Constant.TASK_GET_ROOT_DIR:
                returnMSG(Constant.RETURN_LOG, Constant.LOGTYPE_ROOT_DIR, rootDir);
                break;
            case Constant.TASK_SET_ROOT_DIR:
                setRootDir(intent.getStringExtra("ARG"));
                returnMSG(Constant.RETURN_LOG, Constant.LOGTYPE_ROOT_DIR, rootDir);
                break;
            case Constant.TASK_GET_PORT:
                returnMSG(Constant.RETURN_LOG, Constant.LOGTYPE_PORT, PORT);
                break;
            case Constant.TASK_SET_PORT:
                PORT = intent.getIntExtra("ARG", 8080);
                returnMSG(Constant.RETURN_LOG, Constant.LOGTYPE_PORT, PORT);
                break;
            case Constant.TASK_START_SERVER:
                if (!serverState) {
                    try {
                        start();
                        serverState = true;
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                returnMSG(Constant.RETURN_LOG, Constant.LOGTYPE_STATUS, (serverState?"true":"false"));
                break;
            case Constant.TASK_SERVER_STATE:
                returnMSG(Constant.RETURN_LOG, Constant.LOGTYPE_STATUS, (serverState?"true":"false"));
                break;
        }
    }

    /*
     * 在此傳遞訊息回 activity, 注意 setAction() 要滿足 filter 條件
     * msgType 有三類:
     *   RETURN_LOG:
     *     logType 又有三類: LOGTYPE_ROOT_DIR, LOGTYPE_STATUS, LOGTYPE_TASK
     *   RETURN_CLIENT:
     *   RETURN_SERVER:
     */
    private void returnMSG(int msgType, int logType, String msg) {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(MainActivity.MyBroadcastReceiver.PROCESS_RESPONSE);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        broadcastIntent.putExtra(Constant.FLAG_RETURN, msgType);
        broadcastIntent.putExtra("LOGTYPE", logType);
        broadcastIntent.putExtra("TIME", System.currentTimeMillis());
        broadcastIntent.putExtra("MSG", msg);
        sendBroadcast(broadcastIntent);
    }
    private void returnMSG(int msgType, int logType, int msg) {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(MainActivity.MyBroadcastReceiver.PROCESS_RESPONSE);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        broadcastIntent.putExtra(Constant.FLAG_RETURN, msgType);
        broadcastIntent.putExtra("LOGTYPE", logType);
        broadcastIntent.putExtra("TIME", System.currentTimeMillis());
        broadcastIntent.putExtra("MSG", msg);
        sendBroadcast(broadcastIntent);
    }

    @SuppressWarnings("serial")
    public static final List<String> INDEX_FILE_NAMES = new ArrayList<String>() {
        {
            add("index.html");
            add("index.htm");
        }
    };

    private void printMap(Map<String, String> map, String title) {
        Iterator<String> e = map.keySet().iterator();
        int first = 1;
        while (e.hasNext()) {
            String key = e.next();
            if (first == 1) {
                Log.i(TAG, title + ": '" + key + "' = '" + map.get(key) + "'");
                first = 0;
            }
            else {
                Log.i(TAG, "\t: '" + key + "' = '" + map.get(key) + "'");
            }
        }
    }
    private final static String ALLOWED_METHODS = "GET, POST, PUT, DELETE, OPTIONS, HEAD";
    private final static int MAX_AGE = 42 * 60 * 60;
    final static String DEFAULT_ALLOWED_HEADERS = "origin,accept,content-type";
    public final static String ACCESS_CONTROL_ALLOW_HEADER_PROPERTY_NAME = "AccessControlAllowHeader";

    private String calculateAllowHeaders(Map<String, String> queryHeaders) {
        // 應該從 queryHeaders 來計算，這邊只是用預設值
        return System.getProperty(ACCESS_CONTROL_ALLOW_HEADER_PROPERTY_NAME, DEFAULT_ALLOWED_HEADERS);
    }

    private Response newFixedFileResponse(File file, String mime) throws FileNotFoundException {
        Response res;
        res = newFixedLengthResponse(Response.Status.OK, mime, new FileInputStream(file), (int) file.length());
        res.addHeader("Accept-Ranges", "bytes");
        return res;
    }

    private Response addCORSHeaders(Map<String,String> headers, Response resp, String cors) {
        resp.addHeader("Access-Control-Allow-Origin", cors);
        resp.addHeader("Access-Control-Allow-Headers", calculateAllowHeaders(headers)); // "X-Requested-With, Authorization");
        resp.addHeader("Access-Control-Allow-Credentials", "true");
        resp.addHeader("Access-Control-Allow-Methods", ALLOWED_METHODS);
        resp.addHeader("Access-Control-Max-Age", ""+MAX_AGE);
        return resp;
    }

    private String findIndexFileInDirectory(File directory) {
        for (String fileName : MyServer.INDEX_FILE_NAMES) {
            File indexFile = new File(directory, fileName);
            if (indexFile.isFile()) {
                return fileName;
            }
        }
        return null;
    }

    private String encodeUri(String uri) {
        String newUri = "";
        StringTokenizer st = new StringTokenizer(uri, "/ ", true);
        while (st.hasMoreTokens()) {
            String tok = st.nextToken();
            if ("/".equals(tok)) {
                newUri += "/";
            } else if (" ".equals(tok)) {
                newUri += "%20";
            } else {
                try {
                    newUri += URLEncoder.encode(tok, "UTF-8");
                } catch (UnsupportedEncodingException ignored) {
                }
            }
        }
        return newUri;
    }

    protected String listDirectory(String uri, File f) {
        String heading = "Directory " + uri;
        StringBuilder msg =
            new StringBuilder("<html><head><title>" + heading + "</title><style><!--\n" + "span.dirname { font-weight: bold; }\n" + "span.filesize { font-size: 75%; }\n"
                + "// -->\n" + "</style>" + "</head><body><h1>" + heading + "</h1>");

        String up = null;
        if (uri.length() > 1) {
            String u = uri.substring(0, uri.length() - 1);
            int slash = u.lastIndexOf('/');
            if (slash >= 0 && slash < u.length()) {
                up = uri.substring(0, slash + 1);
            }
        }

        List<String> files = Arrays.asList(f.list(new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {
                return new File(dir, name).isFile();
            }
        }));
        Collections.sort(files);
        List<String> directories = Arrays.asList(f.list(new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {
                return new File(dir, name).isDirectory();
            }
        }));
        Collections.sort(directories);
        if (up != null || directories.size() + files.size() > 0) {
            msg.append("<ul>");
            if (up != null || directories.size() > 0) {
                msg.append("<section class=\"directories\">");
                if (up != null) {
                    msg.append("<li><a rel=\"directory\" href=\"").append(up).append("\"><span class=\"dirname\">..</span></a></b></li>");
                }
                for (String directory : directories) {
                    String dir = directory + "/";
                    msg.append("<li><a rel=\"directory\" href=\"").append(encodeUri(uri + dir)).append("\"><span class=\"dirname\">").append(dir)
                        .append("</span></a></b></li>");
                }
                msg.append("</section>");
            }
            if (files.size() > 0) {
                msg.append("<section class=\"files\">");
                for (String file : files) {
                    msg.append("<li><a href=\"").append(encodeUri(uri + file)).append("\"><span class=\"filename\">").append(file).append("</span></a>");
                    File curFile = new File(f, file);
                    long len = curFile.length();
                    msg.append("&nbsp;<span class=\"filesize\">(");
                    if (len < 1024) {
                        msg.append(len).append(" bytes");
                    } else if (len < 1024 * 1024) {
                        msg.append(len / 1024).append(".").append(len % 1024 / 10 % 100).append(" KB");
                    } else {
                        msg.append(len / (1024 * 1024)).append(".").append(len % (1024 * 1024) / 10000 % 100).append(" MB");
                    }
                    msg.append(")</span></li>");
                }
                msg.append("</section>");
            }
            msg.append("</ul>");
        }
        msg.append("</body></html>");
        return msg.toString();
    }

    protected Response getForbiddenResponse(String s) {
        Response r = newFixedLengthResponse(Response.Status.FORBIDDEN, NanoHTTPD.MIME_PLAINTEXT, "FORBIDDEN: " + s);
        return r;
    }

    protected Response getInternalErrorResponse(String s) {
        Response r = newFixedLengthResponse(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "INTERNAL ERROR: " + s);
        return r;
    }

    protected Response getNotFoundResponse() {
        Response r = newFixedLengthResponse(Response.Status.NOT_FOUND, NanoHTTPD.MIME_PLAINTEXT, "Error 404, file not found.");
        return r;
    }

    private boolean canServeUri(String uri) {
        boolean serveOrNot;
        File f = new File(rootDir, uri);
        serveOrNot = f.exists();
        return serveOrNot;
    }

    /**
     * Serves file from homeDir and its' subdirectories (only). Uses only URI,
     * ignores all headers and HTTP parameters.
     */
    Response serveFile(String uri, Map<String, String> header, File file, String mime) {
        Log.i(TAG, "MyServer.serveFile("+rootDir+","+uri+","+file+")");
        Response res;
        try {
            // Calculate etag
            String etag = Integer.toHexString((file.getAbsolutePath() + file.lastModified() + "" + file.length()).hashCode());

            // Support (simple) skipping:
            long startFrom = 0;
            long endAt = -1;
            String range = header.get("range");
            if (range != null) {
                if (range.startsWith("bytes=")) {
                    range = range.substring("bytes=".length());
                    int minus = range.indexOf('-');
                    try {
                        if (minus > 0) {
                            startFrom = Long.parseLong(range.substring(0, minus));
                            endAt = Long.parseLong(range.substring(minus + 1));
                        }
                    } catch (NumberFormatException ignored) {
                    }
                }
            }

            // get if-range header. If present, it must match etag or else we
            // should ignore the range request
            String ifRange = header.get("if-range");
            boolean headerIfRangeMissingOrMatching = (ifRange == null || etag.equals(ifRange));

            String ifNoneMatch = header.get("if-none-match");
            boolean headerIfNoneMatchPresentAndMatching = ifNoneMatch != null && ("*".equals(ifNoneMatch) || ifNoneMatch.equals(etag));

            // Change return code and add Content-Range header when skipping is
            // requested
            long fileLen = file.length();

            if (headerIfRangeMissingOrMatching && range != null && startFrom >= 0 && startFrom < fileLen) {
                // range request that matches current etag
                // and the startFrom of the range is satisfiable
                if (headerIfNoneMatchPresentAndMatching) {
                    // range request that matches current etag
                    // and the startFrom of the range is satisfiable
                    // would return range from file
                    // respond with not-modified
                    res = newFixedLengthResponse(Response.Status.NOT_MODIFIED, mime, "");
                    res.addHeader("ETag", etag);
                } else {
                    if (endAt < 0) {
                        endAt = fileLen - 1;
                    }
                    long newLen = endAt - startFrom + 1;
                    if (newLen < 0) {
                        newLen = 0;
                    }

                    FileInputStream fis = new FileInputStream(file);
                    fis.skip(startFrom);

                    res = newFixedLengthResponse(Response.Status.PARTIAL_CONTENT, mime, fis, newLen);
                    res.addHeader("Accept-Ranges", "bytes");
                    res.addHeader("Content-Length", "" + newLen);
                    res.addHeader("Content-Range", "bytes " + startFrom + "-" + endAt + "/" + fileLen);
                    res.addHeader("ETag", etag);
                }
            } else {

                if (headerIfRangeMissingOrMatching && range != null && startFrom >= fileLen) {
                    // return the size of the file
                    // 4xx responses are not trumped by if-none-match
                    res = newFixedLengthResponse(Response.Status.RANGE_NOT_SATISFIABLE, NanoHTTPD.MIME_PLAINTEXT, "");
                    res.addHeader("Content-Range", "bytes */" + fileLen);
                    res.addHeader("ETag", etag);
                } else if (range == null && headerIfNoneMatchPresentAndMatching) {
                    // full-file-fetch request
                    // would return entire file
                    // respond with not-modified
                    res = newFixedLengthResponse(Response.Status.NOT_MODIFIED, mime, "");
                    res.addHeader("ETag", etag);
                } else if (!headerIfRangeMissingOrMatching && headerIfNoneMatchPresentAndMatching) {
                    // range request that doesn't match current etag
                    // would return entire (different) file
                    // respond with not-modified

                    res = newFixedLengthResponse(Response.Status.NOT_MODIFIED, mime, "");
                    res.addHeader("ETag", etag);
                } else {
                    // supply the file
                    res = newFixedFileResponse(file, mime);
                    res.addHeader("Content-Length", "" + fileLen);
                    res.addHeader("ETag", etag);
                }
            }
        } catch (IOException ioe) {
            res = getForbiddenResponse("Reading file failed.");
        }

        return res;
    }

    public String getRootDir() { return rootDir; }
    public void setRootDir(String root) { // TODO: 應該判斷 root 為有效目錄
        rootDir = root;
    }

    public static String sudo (String...strings) {
        String res = "";
        DataOutputStream outputStream = null;
        InputStream response = null;

        try {
            // 我測試的系統沒有 su 可以用，所以改用 /system/bin/sh
            // Process su = Runtime.getRuntime().exec("su");
            // Process su = Runtime.getRuntime().exec("/system/bin/su");
            Process su = Runtime.getRuntime().exec("/system/bin/sh");
            outputStream = new DataOutputStream(su.getOutputStream());
            response = su.getInputStream();

            for (String s : strings) {
                outputStream.writeBytes(s + "\n");
                outputStream.flush();
            }

            outputStream.writeBytes("exit\n");
            outputStream.flush();
            try {
                su.waitFor();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            res = readFully(response);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            Closer.closeSilently(outputStream, response);
        }
        return res;
    }
    public static String readFully(InputStream is) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length = 0;
        while ((length = is.read(buffer)) != -1) {
            baos.write(buffer, 0, length);
        }
        return baos.toString("UTF-8");
    }
    public static class Closer{
        public static void closeSilently(Object...xs) {
            for (Object x : xs) {
                try {
                    if (x instanceof Closeable) {
                        ((Closeable) x).close();
                    } else if (x instanceof Socket) {
                        ((Socket)x).close();
                    } else if (x instanceof DatagramSocket) {
                        ((DatagramSocket)x).close();
                    } else {
                        throw new RuntimeException("Cannot close "+x);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private Response respondUpload(IHTTPSession session) {
        if (session.getMethod() == Method.GET) {
            return newFixedLengthResponse(Response.Status.OK, NanoHTTPD.MIME_HTML, uploadFileHTML);
        }
        else {
            String msg = "";
            // mkdir Upload
            File upload = new File(rootDir+"/Upload");
            if (!upload.exists()) upload.mkdir();
            if (!upload.exists()) upload = new File(rootDir);
            Map<String, String>files = new HashMap<String, String>();
            try {
                session.parseBody(files);
                Set<String> keys = files.keySet();
                for (String key:keys) {
                    String name = key;
                    String location = files.get(key);
                    File temp = new File(location);
                    File target = new File(upload.getPath()+"/"+ session.getParms().get("upload"));
                    msg += "upload to "+target.getPath();
                    InputStream in = new FileInputStream(temp);
                    OutputStream out = new FileOutputStream(target);
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                    in.close();
                    out.close();
                }
            }
            catch (IOException e1) {
                e1.printStackTrace();
            }
            catch (ResponseException e1) {
                e1.printStackTrace();
            }
            return newFixedLengthResponse(Response.Status.OK, NanoHTTPD.MIME_HTML, msg);
        }
    }

    private Response respondExec(IHTTPSession session) {
        String msg = "";
        if (session.getMethod() == Method.POST) {
            try {
                session.parseBody(new HashMap<String, String>());
            } catch (ResponseException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Map<String, String> parms = session.getParms();     // url 裡面的 ? 後面的參數
        String cmd = parms.get("cmd");
        String res = sudo(cmd);
        if (res.isEmpty()) {
            msg = "no SU found to exec cmd "+cmd;
        }
        else {
            msg = "exec " + cmd + ": <br /><PRE>" + res + "</PRE>";
        }
        return newFixedLengthResponse(Response.Status.OK, NanoHTTPD.MIME_HTML, msg);
    }

    private Response defaultRespond(Map<String, String> headers, IHTTPSession session, String uri) {
        Log.i(TAG, "MyServer.defaultRespond("+rootDir+","+uri+") with method "+session.getMethod());
        // Remove URL arguments
        uri = uri.trim().replace(File.separatorChar, '/');
        if (uri.indexOf('?') >= 0) {
            uri = uri.substring(0, uri.indexOf('?'));
        }

        // Prohibit getting out of current directory
        if (uri.contains("../")) {
            return getForbiddenResponse("Won't serve ../ for security reasons.");
        }

        boolean serveOrNot = canServeUri(uri);
        if (!serveOrNot) {
            if (uri.startsWith("/u")) {
                return respondUpload(session);
            }
            else if (uri.startsWith("/e")) {
                return respondExec(session);
            }
            else return getNotFoundResponse();
        }

        // 如果 Uri 是目錄而且不是用 '/' 結尾的話，則加上 '/' 再查詢一次
        File f = new File(rootDir, uri);
        if (f.isDirectory() && !uri.endsWith("/")) {
            uri += "/";
            Response res =
                newFixedLengthResponse(Response.Status.REDIRECT, NanoHTTPD.MIME_HTML, "<html><body>Redirected: <a href=\"" + uri + "\">" + uri + "</a></body></html>");
            res.addHeader("Location", uri);
            return res;
        }

        if (f.isDirectory()) {
            // 如果是目錄，先查一查是否有 index.html, index.htm
            String indexFile = findIndexFileInDirectory(f);
            if (indexFile == null) {
                if (f.canRead()) {
                    // 如果沒有 index file, url 指向的目錄又可讀取的話，傳回該目錄下的檔案名
                    return newFixedLengthResponse(Response.Status.OK, NanoHTTPD.MIME_HTML, listDirectory(uri, f));
                } else {
                    return getForbiddenResponse("No directory listing.");
                }
            } else {
                return respond(headers, session, uri + indexFile);
            }
        }
        String mimeTypeForFile = getMimeTypeForFile(uri);
        Response response = null;
        response = serveFile(uri, headers, f, mimeTypeForFile);
        return response != null ? response : getNotFoundResponse();
    }

    private Response respond(Map<String, String> headers, IHTTPSession session, String uri) {
        // 似乎應該先處理 cors, 這是『Cross Site Request』規定的 Cross-Origin Resource Sharing
        Response r;
        String cors = null;
        if (cors != null && Method.OPTIONS.equals(session.getMethod())) {
            Log.i(TAG, "MyServer.respond("+rootDir+","+uri+") with cors");
            r = new NanoHTTPD.Response(Response.Status.OK, MIME_PLAINTEXT, null, 0);
        } else {
            Log.i(TAG, "MyServer.respond("+rootDir+","+uri+") without cors");
            r = defaultRespond(headers, session, uri);
        }

        if (cors != null) {
            r = addCORSHeaders(headers, r, cors);
        }
        return r;
    }

    @Override
    public Response serve(IHTTPSession session) {
        String uri = session.getUri();
        Log.i(TAG, "MyServer.serve("+rootDir+","+uri+")");

        Map<String, String> headers = session.getHeaders(); // 最重要的是 "host", "http-client-ip" = "remote-addr"
//        printMap(headers, "HEADER");
//        Map<String, String> parms = session.getParms();     // url 裡面的 ? 後面的參數
//        printMap(parms, "PARM");

        return respond(Collections.unmodifiableMap(headers), session, uri);
    }
}
