package com.wade.webserver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by wade on 2016/9/29.
 */
public class OnBoot extends BroadcastReceiver
{

    @Override
    public void onReceive(Context context, Intent intent)
    {
        // Create Intent
        Intent serviceIntent = new Intent(context, MyServer.class);
        serviceIntent.putExtra(Constant.FLAG_TASK, Constant.TASK_START_SERVER);
        // Start service
        context.startService(serviceIntent);

    }

}