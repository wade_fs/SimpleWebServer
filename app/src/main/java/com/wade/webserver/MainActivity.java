package com.wade.webserver;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    private final String TAG = "SimpleWebServer";

    TextView mTextView;
    private MyBroadcastReceiver receiver;
    private Menu mMenu;
    private boolean bWebServiceState = false;
    private String rootDir;
    private int port;

    /*
     * 需要在 onCreate() 中註冊並設定要滿足過濾條件的 BroadcastReceiver
     * 這樣才有辦法收到 MyServer 傳上來的訊息，
     * 這些收到的訊息由上面的 MyBroadcastReceiver 在 onReceive() 中處理
     * 這邊看起來是單向接收訊息，要傳給 MyServer 也很簡單，
     *   在 startService(intent) 中的 intent 傳下去即可
     */
    public class MyBroadcastReceiver extends BroadcastReceiver {
        public static final String PROCESS_RESPONSE = "PROCESS_RESPONSE";

        @Override
        public void onReceive(Context context, Intent intent) {
            int flagReturn = intent.getIntExtra(Constant.FLAG_RETURN, Constant.RETURN_LOG);

            switch (flagReturn) {
                case Constant.RETURN_LOG: // LOG 會有 logtype
                    int logType = intent.getIntExtra("LOGTYPE", Constant.LOGTYPE_ROOT_DIR);
                    switch (logType) {
                        case Constant.LOGTYPE_ROOT_DIR:
                            rootDir = intent.getStringExtra("MSG");
                            appendMsg("Change root dir to "+rootDir);
                            break;
                        case Constant.LOGTYPE_STATUS:
                            bWebServiceState = intent.getStringExtra("MSG").contains("true");
                            if (!bWebServiceState) appendMsg("Web Service is "+(bWebServiceState?"started":"stopped"));
                            else showAbout();
                            break;
                        case Constant.LOGTYPE_PORT:
                            port = intent.getIntExtra("MSG", 8080);
                            appendMsg("listen port at "+port);
                            break;
                    }
                    break;
                case Constant.RETURN_CLIENT:
                    break;
                case Constant.RETURN_SERVER:
                    break;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTextView = (TextView)findViewById(R.id.msg_view);
        /*
         * 底下註冊並設定要滿足過濾條件的 BroadcastReceiver
         * 這樣才有辦法收到 MyServer 傳上來的訊息，
         * 這些收到的訊息由上面的 MyBroadcastReceiver 在 onReceive() 中處理
         */
        IntentFilter intentFilter = new IntentFilter(MyBroadcastReceiver.PROCESS_RESPONSE);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        receiver = new MyBroadcastReceiver();
        registerReceiver(receiver, intentFilter);
        sendTask(Constant.TASK_SERVER_STATE);
        sendTask(Constant.TASK_GET_ROOT_DIR);
        sendTask(Constant.TASK_GET_PORT);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        mMenu = menu;
        return true;
    }

    public void appendMsg(String msg) {
        mTextView.append("\n"+msg);
    }

    private void showAbout() {
        if (bWebServiceState) {
            mTextView.setText("Serve at http://" + Utils.getIPAddress(true) + ":" + port + "\n" +
                    "Root Dir: " + rootDir + "\n" +
                    "Author: mailto://wade.fs@gmail.com\n" +
                    "Web Site: https://wadefs.blogspot.com");
        }
        else {
            mTextView.setText("SimpleWebServer are not started. \n" +
                    "Author: mailto://wade.fs@gmail.com\n" +
                    "Web Site: https://wadefs.blogspot.com");
        }
        Linkify.addLinks(mTextView, Linkify.ALL);
    }

    private void sendTask(int task) {
        Intent intent = new Intent(MainActivity.this, MyServer.class);
        intent.putExtra(Constant.FLAG_TASK, task);
        startService(intent);
    }
    private void sendTask(int task, String arg) {
        Intent intent = new Intent(MainActivity.this, MyServer.class);
        intent.putExtra(Constant.FLAG_TASK, task);
        intent.putExtra("ARG", arg);
        startService(intent);
    }

    private void startServer() {
        sendTask(Constant.TASK_START_SERVER);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_about) {
            showAbout();
            return true;
        }
        else if (id == R.id.action_exit) {
            finish();
        }
        else if (id == R.id.action_setRoot) {
            FileChooser fileChooser = new FileChooser(this, rootDir).setFileListener(
                    new FileChooser.FileSelectedListener() {
                        @Override
                        public void fileSelected(File file) {
                            sendTask(Constant.TASK_SET_ROOT_DIR, file.getPath());
                        }
                    }
            );
            fileChooser.showDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        mTextView = (TextView)findViewById(R.id.msg_view);
        startServer();
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(receiver);
        super.onDestroy();
    }
}
